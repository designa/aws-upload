import AWS from 'aws-sdk';
import { createReadStream } from 'fs';

AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION
})

const s3 = new AWS.S3();

type ParamsFile = {
  file: any;
  key: string;
  path: string;
  delete: boolean;
  newName: string;
}

type UploadParams = {
  Bucket: string;
  Key: string;
  Body: any;
  ContentType: any;
  ContentDisposition: string;
  ACL?: string
}

type PresignedPostParams = {
  Bucket: string;
  Fields: {
    key: string,
    acl?: string
  };
  Conditions: any;
  Expires: number;
}

export default {
  upload: async (paramsFile: ParamsFile, acl?: string): Promise<string> => new Promise((resolve, reject) => {
    try {
      const stream = createReadStream(paramsFile.file.path);
      const url_string = `${encodeURIComponent(paramsFile.key)}/${encodeURIComponent(paramsFile.path)}/${encodeURIComponent(paramsFile.newName)}`;
      const params: UploadParams = {
        Bucket: process.env.AWS_BUCKET || '',
        Key: url_string,
        Body: stream,
        ContentType: paramsFile.file.mimetype,
        ContentDisposition: `inline; filename=${encodeURIComponent(paramsFile.newName)}`
      };
      if (acl !== undefined) {
        params.ACL = acl
      }
      s3.upload(params, (err: unknown, data: AWS.S3.ManagedUpload.SendData) => {
        if (err) {
          reject({
            error: err,
            params: params
          });
        } else {
          resolve(data.Location);
        }
      });
    } catch (err: unknown) {
      const { message } = err as Error;
      reject(err);
    }
  }),
  getPresignedUrl: async (key: string, path: string, file: string): Promise<string> => new Promise((resolve, reject) => {
    try {
      const url_string = `${encodeURIComponent(key)}/${encodeURIComponent(path)}`;
      s3.getSignedUrl('getObject', {
        Bucket: process.env.AWS_BUCKET || '',
        Key: url_string,
        Expires: 300
      }, (err: unknown, url: string) => {
        if (err) {
          reject(err);
        } else {
          resolve(url);
        }
      });

    } catch (err: unknown) {
      const { message } = err as Error;
      reject(message);
    }
  }),
  getPresignedPost: async (key: string, path: string, file: string, acl?: string, content_length_range?: number): Promise<string> => new Promise((resolve, reject) => {
    try {
      var params: PresignedPostParams = {
        Bucket: process.env.AWS_BUCKET || '',
        Fields: {
          key: key + '/' + path + '/' + file
        },
        Conditions: [
          ["content-length-range", 0, content_length_range ? content_length_range : 5000000], //Defines the maximum allowed object size.
        ],
        Expires: 3600
      }
      if (acl !== undefined) {
        params.Fields.acl = acl
      }
      s3.createPresignedPost(params, function (err, data) {
        if (err) {
          console.error('Presigning post data encountered an error', err);
          reject(err);
        } else {
          resolve(JSON.stringify(data));
        }
      })
    }
    catch (err: unknown) {
      const { message } = err as Error;
      reject(message);
    }
  }),
  getObject: async (key: string, path: string, file: string): Promise<string> => new Promise((resolve, reject) => {
    try {
      const url_string = `${encodeURIComponent(key)}/${encodeURIComponent(path)}/${encodeURIComponent(file)}`;
      s3.getSignedUrl('getObject', {
        Bucket: process.env.AWS_BUCKET || '',
        Key: url_string,
        Expires: 60
      }, (err: unknown, url: string) => {
        if (err) {
          reject(err);
        } else {
          resolve(url);
        }
      });

    } catch (err: unknown) {
      const { message } = err as Error;
      reject(message);
    }
  })
};
